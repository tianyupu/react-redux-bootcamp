# React/Redux Bootcamp
## Tian Pu

---
# Agenda

* Setting the scene
* React
* Redux
* react-redux
* Exercise and Q&A
* Resources

---
# Setting the scene

--
## What is going to be covered

--
![Overview of what we will cover](overview.svg)

--
## What is not going to be covered

* Communicating with server (fetching data, saving data, validation)
* Analytics
* Asynchronous, time-sensitive operations

---
# React overview

--
## Motivation

Once upon a time, this is how we used to write our frontend applications:

```html
<header>  
    <div class="name">
        Not Logged In
    </div>
</header>  
```

```javascript
$.post('/login', credentials, function(user) {
    // Modify the DOM here, but who knows where else we'll modify this same piece of DOM?
    $('header .name').text(user.name);
});
```

--
How would we write the view part of that in React?

```javascript
render() {
    const { name } = this.props;
    return (
        <header>
            {name || <span>Not Logged In</span>}
        </header>
    );
}
```

--
## Key idea

The view is a pure function of state and props.

--
Stated another way: given some set of inputs, we will _always_ see the same view
for that set of inputs.

--
## Defining a React component - todo list example

```javascript
export class TodoItem extends Component {
    render() {
        const { todo } = this.props;
        return todo.isEditing ? this.renderEditView(todo) : this.renderReadView(todo);
    }
}
```

```javascript
export class TodoList extends Component {
    render() {
        const { todos, onNewTodoClick } = this.props;
        return (
            <div>
                <h1>Todo List</h1>
                <div>
                    {todos.map((todo) => (
                        <TodoItem id={todo.id} key={todo.id} />
                    ))}
                </div>
                <button onClick={onNewTodoClick}>New Todo</button>
            </div>
        );
    }
}
```

--
## Props and state

* Props: once passed to the component, cannot be changed from within the
component
* State: can be changed within the component by calling `this.setState(obj)`

--
Changes to either props or state will cause the `render()` method of the
component to be called, forcing a redraw of the view.

--
## JSX

What is this strange syntax?

```javascript
const element = <p>Hello world!</p>;
```

--
It's neither a string nor HTML, but JSX (a syntax extension to JavaScript).

--
JSX is compiled into `React.createElement(component, props, ...)`.

For example:

```javascript
<TodoItem id={42}>
    This is a todo.
</TodoItem>
```

and

```javascript
React.createElement(
    TodoItem,
    {id: 42},
    'This is a todo.'
)
```

are equivalent.

--
## Recap

* UI as a pure function of state and props
* No direct DOM manipulation, _ever_
* JSX makes components easier to understand
* React is only the view layer!

---
# Redux overview

--
## Motivation

More complex frontend apps

More state to manage

More complexity

Less reasonability of state

...

Bad Times™

--
## Key idea

Redux attempts to make state mutations predictable. How?

* Entire app state is stored in an object inside a single **store**.
* The only way to change the state is to emit an **action** (an object
describing what happened).
* To specify how the actions transform the state, write pure **reducers**.

--
## Redux example - todo list

Example app state for a todo list app:

```javascript
{
    todos: [
        {
            id: 1,
            text: 'Learn about React and Redux',
            isCompleted: true,
            isEditing: false
        },
        {
            id: 2,
            text: 'Win ShipIt',
            isCompleted: false,
            isEditing: false
        }
    ]
}
```

--
To change the state, we need to dispatch an **action**:

```javascript
{
    type: 'TODO_EDIT',
    payload: { id: 1, value: 'Get coffee' }
}
```

This enforces that every mutation is explicitly described.

--
To bring the state and actions together, write a **reducer**:

```javascript
const initialState = { todos: [] };
function reducer(state = initialState, action) {
    switch (action.type) {
        case 'TODO_EDIT':
            const { id, value } = payload;
            const editedTodo = state.todos.find(todo => todo.id === id);

            return {
                ...state,
                todos: [
                    ...state.todos.filter(todo => todo.id !== id),
                    {
                        ...editedTodo,
                        text: value
                    }
                ]
            };
            
        default:
            return state;
    }
}
```

--
New state after the update:

```javascript
{
    todos: [
        {
            id: 1,
            text: 'Get coffee', // this got updated in the reducer
            isCompleted: true,
            isEditing: false
        },
        {
            id: 2,
            text: 'Win ShipIt',
            isCompleted: false,
            isEditing: false
        }
    ]
}
```

--
Note that all we used was plain JavaScript!

--
## Using Redux

* Store
* Actions
* Reducers

--
### Using Redux - Store

* Holds app state
* Allows access to state
* Allows state to be updated
* Subscribe/unsubscribe to changes in the state

--
We don't actually interact with the store directly very much,
as we'll see in the next section on `react-redux`. 

The key point for us is that we can `dispatch` actions, or
intentions to modify the state held in the store.

```javascript
store.dispatch(editTodo({ id: 1, value: 'Get coffee' }));
```

--
### Using Redux - Actions

Actions send data from app to store -- they are the only source of
information for the store.

Conceptually, as we saw in the example above, they look like:

```javascript
{
    type: 'TODO_EDIT',
    payload: { id: 1, value: 'Get coffee' }
}
```

--
How are they created in Redux?

First, define an **action type** for your action:

```javascript
export const TODO_EDIT = 'TODO_EDIT';
```

Then, define an **action creator** for your action:

```javascript
export const editTodo = ({ id, value }) => ({
    type: Types.TODO_EDIT,
    payload: { id, value }
});
```

Finally, we call the store's `dispatch`, passing it the action creator, when
we want to change the state of the app.

--
### Using Redux - Reducers

Reducers specify how the app state changes in response to an action.

--
First, consider the shape of the state which is appropriate to your app:

```javascript
{
    todos: [
        {
            id: 1,
            text: 'Learn about React and Redux',
            isCompleted: true,
            isEditing: false
        },
        {
            id: 2,
            text: 'Win ShipIt',
            isCompleted: false,
            isEditing: false
        }
    ]
}
```

--
A reducer is just a function with the signature

```
(previousState, action) => newState
```

--
How do we write one?

Here's a reducer that initially doesn't do anything:

```javascript
const initialState = {
    todos: []
};
function reducer(state = initialState, action) {
    return state;
}
```

--
Let's get it to handle a particular action type:

```javascript
function reducer(state = initialState, action) {
    switch (action.type) { // all actions will have a type
        case 'TODO_EDIT':
            const { id, value } = payload; // this is specific to the particular action creator
            const editedTodo = state.todos.find(todo => todo.id === id);

            // it's important that we don't modify `state` in any way, so we use the spread
            // operator to copy over the contents of the current state into a new object
            return {
                ...state,
                todos: [
                    ...state.todos.filter(todo => todo.id !== id),
                    {
                        ...editedTodo,
                        text: value
                    }
                ]
            };
        
        // add more cases here for different action types
            
        default:
            // always return the current state if we don't know what to do with an action
            return state; 
    }
}
```

--
When the reducer gets too big, we can write separate reducers to manage
different parts of the state and combine the reducers using
`combineReducers` from Redux.

--
## Recap

* Single source of truth
* Read-only state
* "Change" state using pure functions

---
# react-redux

--
## Motivation

So far we've talked about:
* Components that render the state that they're given (React)
* Updating the application state (Redux)

--
How do we get the application state into the components?

--
This is where `react-redux` comes in.

--
First, some terms:
* Presentational or 'dumb' components
* Container or 'smart' components

--
```javascript
export class TodoListDumb extends Component {
    render() {
        const { todos, onNewTodoClick } = this.props;
        return (
            <div>
                <h1>Todo List</h1>
                <div>
                    {todos.map((todo) => <TodoItem id={todo.id} key={todo.id} />)}
                </div>
                <button onClick={onNewTodoClick}>New Todo</button>
            </div>
        );
    }
}
```

```javascript
export default class TodoListSmart extends Component {
    constructor(props) {
        this.store = createStore();
        this.store.subscribe(() => { this.setState(this.store.getState()); }
    }
    
    render() {
        const { todos } = this.state;
        return (
            <TodoListDumb todos={todos} onNewTodoClick={() => this.store.dispatch(addTodo(id))} />
        );
    }
}
```

--
## Key idea

Container ('smart') components are just React components that use
`store.subscribe()` to read from the state and supply props to a
presentational ('dumb') component.

This allows us to use the same dumb component in multiple apps
with different Redux stores.

--
We can use `connect()` from `react-redux` to generate a smart
component for us:

```javascript
export default connect(
    state => ({
        todos: state.todos
    }),
    dispatch => ({
        onNewTodoClick: () => dispatch(addTodo(generateId()))
    })
)(TodoListDumb);
```

vs
```javascript
export default class TodoListSmart extends Component {
    constructor(props) {
        this.store = createStore();
        this.store.subscribe(() => { this.setState(this.store.getState()); }
    }
    
    render() {
        const { todos } = this.state;
        return (
            <TodoListDumb todos={todos} onNewTodoClick={() => this.store.dispatch(addTodo(id))} />
        );
    }
}
```

--
## Recap

* Write reusable dumb components by hand
* Wire up the dumb component with the state of the app using
`connect` which creates a wrapping smart component

---
# Summary

* React is just the view layer: view as a function of props and state
* Redux is just the state of the frontend app: a way to reason about app
state changes
* `connect` from `react-redux` wires up the two
* None of them deal with side-effects such as server communication --
these are done elsewhere

---
# Exercise and Q&A

```bash
brew install yarn
git clone https://bitbucket.org/tianyupu/react-redux-bootcamp.git
yarn && yarn run exercise
```

---
# Resources

--
## React

https://facebook.github.io/react/tutorial/tutorial.html#what-is-react

http://blog.andrewray.me/reactjs-for-stupid-people/

https://facebook.github.io/react/docs/react-component.html

--
## Redux

http://redux.js.org/

--
## react-redux

https://github.com/reactjs/react-redux

http://redux.js.org/docs/basics/UsageWithReact.html

---
# Thank you!

Please take a minute to leave your feedback anonymously at:
https://goo.gl/forms/VAredP0uuJeutott1
