var webpack = require('webpack');
var path = require('path');

module.exports = {
    entry: [
        './exercise/entry'
    ],
    devtool: 'cheap-module-eval-source-map',
    output: {
        path: path.resolve(__dirname, 'exercise'),
        filename: 'bundle.js',
        publicPath: '/exercise'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'react-hot-loader',
                exclude: /node_modules/
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                query: {
                    cacheDirectory: true
                },
                exclude: /node_modules/
            }
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({
            'Promise': 'core-js/es6/promise',
            'fetch': 'imports?this=>global!exports?global.fetch!whatwg-fetch'
        })
    ]
};
