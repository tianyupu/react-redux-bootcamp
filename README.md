# React/Redux Bootcamp

A bootcamp to get developers started with React and Redux.

## View slides online

[Hosted on heroku](https://react-redux-bootcamp.herokuapp.com/)

## Running slides locally

1. Install `node` and `npm` (or `yarn`), and then run `npm install`
(or `yarn`) in the root of the repository. It is highly recommended
to use `yarn` instead of `npm`.
1. Once that's done, start up a local server to view the slides by typing
`npm run edit-slides` (or `yarn run edit-slides`).
1. Navigate to [http://localhost:8000](http://localhost:8000)

## Editing slides

1. Update `index.html` and `slides.md` in `src/` as needed: `index.html`
controls the scripts on the page, and `slides.md` controls the content of
the actual slides.
1. No need to restart the local server -- it will watch for your changes and
refresh the browser accordingly, even if you have
[http://localhost:8000](http://localhost:8000) open when edits are made.

## Running the exercise

The repository also comes with a very barebones todo list app, for the
purposes of illustrating the content of the bootcamp. You can run it by typing
`npm run exercise` (or `yarn run exercise`) and navigating to
[http://localhost:8080](http://localhost:8080). All changes to files in the
`exercise/` folder will be loaded automatically without needing to refresh the
browser.

Tests for the exercise can be run with `npm run exercise:test`
(`yarn run exercise:test`).

## Contributing
Suggestions for content are welcome! Please open a PR or raise an issue.

## License
MIT
