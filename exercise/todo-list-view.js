import React, { Component } from 'react';
import { connect } from 'react-redux';
import TodoItem from './todo-item-view';
import { addTodo } from './action-creator';

function generateId() {
    return Math.floor(Math.random() * 1000000);
}

export class TodoList extends Component {
    render() {
        const { todos, onNewTodoClick } = this.props;

        return (
            <div>
                <h1>Todo List</h1>
                <div>
                    {todos.map((todo) => (
                        <TodoItem id={todo.id} key={todo.id} />
                    ))}
                </div>
                <div>
                    <button onClick={onNewTodoClick}>New Todo</button>
                </div>
            </div>
        );
    }
}

export default connect(
    state => ({
        todos: state.todos
    }),
    dispatch => ({
        onNewTodoClick: () => dispatch(addTodo(generateId()))
    })
)(TodoList);
