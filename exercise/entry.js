import React from 'react';
import { render } from 'react-dom';
import TodoApp from './app';

render(
    <TodoApp />, document.getElementById('app')
);
