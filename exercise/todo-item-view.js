import React, { Component } from 'react';
import { connect } from 'react-redux';
import { editTodo, saveTodo } from './action-creator';

export class TodoItem extends Component {
    renderCheckbox = (todo) => (
        <input type="checkbox" id={`${todo.id}`} value="" checked={todo.isCompleted ? "checked" : false} />
    );

    renderReadView = (todo) => (
        <div>
            {this.renderCheckbox(todo)}
            <label>{todo.text}</label>
        </div>
    );

    renderEditView = (todo) => {
        return (
            <div>
                {this.renderCheckbox(todo)}
                <input type="text" value={todo.text} onChange={(e) => this.props.onChange(e.target.value)} />
                <button onClick={this.props.onClick}>Save</button>
            </div>
        )
    };

    render() {
        const { todo } = this.props;

        return todo.isEditing ? this.renderEditView(todo) : this.renderReadView(todo);
    }
}

export default connect(
    (state, ownProps) => ({
        todo: state.todos.find(todo => todo.id === ownProps.id)
    }),
    (dispatch, ownProps) => ({
        onChange: value => dispatch(editTodo({ id: ownProps.id, value })),
        onClick: () => dispatch(saveTodo(ownProps.id))
    })
)(TodoItem);
