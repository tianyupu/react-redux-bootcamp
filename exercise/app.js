import React, { Component } from 'react';
import { Provider } from 'react-redux';
import createStore from './store';
import TodoList from './todo-list-view';

export default class TodoApp extends Component {
    constructor(props) {
        super(props);

        this.store = createStore();
    }

    render() {
        return (
            <Provider store={this.store}>
                <TodoList />
            </Provider>
        );
    }
}