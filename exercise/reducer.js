import { TODO_ADD, TODO_EDIT, TODO_SAVE } from './action-type';

export const presetTodos = [
    {
        id: 1,
        text: 'Learn about React and Redux',
        isCompleted: true,
        isEditing: false
    },
    {
        id: 2,
        text: 'Win ShipIt',
        isCompleted: false,
        isEditing: false
    }
];

const initialState = {
    todos: presetTodos
};

export default (state = initialState, action) => {
    const { type, payload } = action;

    switch (type) {
        case TODO_ADD:
            return {
                ...state,
                todos: [
                    ...state.todos,
                    {
                        id: payload,
                        text: '',
                        isCompleted: false,
                        isEditing: true
                    }
                ]
            };

        case TODO_EDIT:
            const { id, value } = payload;
            const editedTodo = state.todos.find(todo => todo.id === id);

            return {
                ...state,
                todos: [
                    ...state.todos.filter(todo => todo.id !== id),
                    {
                        ...editedTodo,
                        text: value
                    }
                ]
            };

        case TODO_SAVE:
            const savedTodo = state.todos.find(todo => todo.id === payload);
            return {
                ...state,
                todos: [
                    ...state.todos.filter(todo => todo.id !== payload),
                    {
                        ...savedTodo,
                        isEditing: false
                    }
                ]
            };

        default:
            return state;
    }
};
