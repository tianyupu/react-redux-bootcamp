# Exercise: Todo List Example

## Background: before you begin

This is a very simple implementation of a todo list app. There is only one
todo list, and it comes with two pre-filled todo items. You can add as many
todo items as you like, but as soon as you add one, you can't edit it. You
also can't mark any items as done or not done yet.

There is no persistence, so refreshing the page will lose all your changes
and you'll go back to seeing the two default items.

Feel free to dig around the code at this stage and see if you can understand
how things fit together at this stage. Think about what we covered with React
components, Redux actions and reducers, and using `connect` from `react-redux`
to hook up our view to our application state.

The goal of the example is to consolidate the content of the bootcamp. Let's
get started by implementing some additional features!

## Features to implement

1. Toggling of todo items - I want to be able to tick and untick an item
1. Editing the text of a todo item - if I've made a mistake entering an item,
I want to be able to correct that
1. Deleting a todo item - if I don't want to work on an item anymore, I want
to remove it from the list
1. Editing of the name of the todo list - I might want to name my list
something more useful than just 'Todo List'

## Hints

1. When trying to implement a new feature, think first about the state that
the application should have, and then think about how the view will change
based on the state.
1. What changes do I need to make to the state, and how will I describe those
changes in actions? When and where will I dispatch those actions? How will
I then take those actions and use them to update my application state?

## Testing your changes

We write tests for both our components and our reducers. However, if the
component doesn't have any complex or conditional logic, we don't write a
test for it. Reducers, which are just functions with the signature
`(currentState, action) => newState`, are tested by passing it a state that
we specify, plus an action, and then asserting that the returned new state
is of the shape we expect. For each of your new actions, try to write a
test for each of them in `reducer.test.js`.