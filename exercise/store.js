import { createStore } from 'redux';
import composeWithDevtools from './util/compose-with-dev-tools';
import rootReducer from './reducer';

export default initialState => createStore(
    rootReducer,
    initialState,
    composeWithDevtools({ name: 'Simple Todo List' })()
);