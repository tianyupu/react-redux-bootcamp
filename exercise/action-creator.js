import * as Types from './action-type';

export const addTodo = (id) => ({
    type: Types.TODO_ADD,
    payload: id
});

export const editTodo = ({ id, value }) => ({
    type: Types.TODO_EDIT,
    payload: { id, value }
});

export const saveTodo = (id) => ({
    type: Types.TODO_SAVE,
    payload: id
});
