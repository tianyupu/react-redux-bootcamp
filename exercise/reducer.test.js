import { describe, it } from 'mocha';
import { expect } from 'chai';
import reducer, { presetTodos } from './reducer';
import { addTodo, editTodo, saveTodo } from './action-creator';

describe('Reducer', () => {
    it('should initially be an object with a \'todos\' key which contains only preset todos', () => {
        const uselessAction = {
            type: 'SOME_TEST_ACTION',
            payload: null
        };

        const initialState = reducer(undefined, uselessAction);

        expect(initialState).to.eql({ todos: presetTodos });
    });

    it('should add a draft todo with empty text body when adding a new todo', () => {
        const currentState = {
            todos: presetTodos
        };

        const newState = reducer(currentState, addTodo(42));

        expect(newState.todos).to.deep.equal([
            ...presetTodos,
            {
                id: 42,
                isCompleted: false,
                isEditing: true,
                text: ''
            }
        ]);
    });

    it('should update text of todo when editing a todo', () => {
        const currentState = {
            todos: [
                ...presetTodos,
                {
                    id: 42,
                    isCompleted: false,
                    isEditing: true,
                    text: 'some text'
                }
            ]
        };

        const newState = reducer(currentState, editTodo({ id: 42, value: 'some updated text' }));

        expect(newState.todos).to.deep.equal([
            ...presetTodos,
            {
                id: 42,
                isCompleted: false,
                isEditing: true,
                text: 'some updated text'
            }
        ]);
    });

    it('should mark the todo as no longer a draft when saving a todo', () => {
        const currentState = {
            todos: [
                ...presetTodos,
                {
                    id: 42,
                    isCompleted: false,
                    isEditing: true,
                    text: 'some text'
                }
            ]
        };

        const newState = reducer(currentState, saveTodo(42));

        expect(newState.todos).to.deep.equal([
            ...presetTodos,
            {
                id: 42,
                isCompleted: false,
                isEditing: false,
                text: 'some text'
            }
        ]);
    });
});
